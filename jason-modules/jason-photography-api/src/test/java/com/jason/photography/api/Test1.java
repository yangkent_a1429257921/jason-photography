package com.jason.photography.api;

import com.jason.common.thread.ThreadPoolUtil;
import com.jason.photography.api.utl.SpiderUtil;

import java.util.Arrays;
import java.util.List;
/**
 * TODO
 *
 * @author guozhongcheng
 * @since 2023/6/13
 **/
public class Test1 {
    private static final List<String> URL_LIST = Arrays.asList(
//            " https://www.xr05.vip/XiuRen/8910.html",
//            "   https://www.xr05.vip/XiuRen/9720.html",
//            " https://www.xr05.vip/XiuRen/9743.html",
//            "  https://www.xr05.vip/XiuRen/9705.html",
//            "   https://www.xr05.vip/XiaoYu/9027.html",
//            "   https://www.xr05.vip/XiaoYu/9078.html",
//            "  https://www.xr05.vip/XiuRen/14800.html",
//            "  https://www.xr05.vip/XiuRen/14808.html",
//            "    https://www.xr05.vip/XiuRen/14807.html",
//            "    https://www.xr05.vip/XiuRen/14600.html",
//            "   https://www.xr05.vip/XiuRen/14417.html",
//            " https://www.xr05.vip/XiuRen/14284.html",
//            "    https://www.xr05.vip/XiuRen/14281.html",
//            " https://www.xr05.vip/XiuRen/14326.html",
//            "   https://www.xr05.vip/XiuRen/13580.html",
//            "    https://www.xr05.vip/XiuRen/13826.html",
//            "      https://www.xr05.vip/XiuRen/12556.html",
//            "       https://www.xr05.vip/XiuRen/12548.html",
//            "     https://www.xr05.vip/XiuRen/12487.html",
//            "    https://www.xr05.vip/XiuRen/11421.html",
//            "    https://www.xr05.vip/XiuRen/11365.html",
//            "    https://www.xr05.vip/XiuRen/11316.html",
//            "   https://www.xr05.vip/XiuRen/10749.html",
//            "      https://www.xr05.vip/XiuRen/10748.html",
//            "    https://www.xr05.vip/XiuRen/10142.html",
//            " https://www.xr05.vip/XiuRen/10095.html"
//            "    https://www.xr05.vip/IMiss/12561.html",
//            "       https://www.xr05.vip/XiuRen/9470.html",
//            "   https://www.xr05.vip/XiuRen/14695.html",
//            "    https://www.xr05.vip/XiuRen/14720.html",
//            "    https://www.xr05.vip/XiuRen/13555.html",
//            "    https://www.xr05.vip/XiuRen/13515.html",
//            "  https://www.xr05.vip/XiuRen/13546.html",
//            "  https://www.xr05.vip/XiuRen/13773.html",
//            "    https://www.xr05.vip/YouMi/7733.html",
//            "   https://www.xr05.vip/XiuRen/9726.html",
//            "   https://www.xr05.vip/XiuRen/9933.html",
//            "   https://www.xr05.vip/XiuRen/9800.html",
//            "   https://www.xr05.vip/IMiss/10002.html",
//            "   https://www.xr05.vip/XiuRen/14814.html",
//            "  https://www.xr05.vip/XiaoYu/14401.html",
//            "   https://www.xr05.vip/XiuRen/13216.html",
//            "   https://www.xr05.vip/XiuRen/14815.html",
//            "   https://www.xr05.vip/IMiss/12497.html",
//            "   https://www.xr05.vip/IMiss/9968.html",
//            "  https://www.xr05.vip/XiuRen/12970.html",
//            "   https://www.xr05.vip/XiuRen/13890.html",
//            "   https://www.xr05.vip/XiuRen/12166.html",
//            "    https://www.xr05.vip/XiuRen/13300.html",
//            "   https://www.xr05.vip/XiuRen/13388.html"

//            " https://www.xr05.vip/XiuRen/12537.html",
//            "    https://www.xr05.vip/XiuRen/14100.html",
//            "    https://www.xr05.vip/XiuRen/13146.html",
//            "    https://www.xr05.vip/XiuRen/12691.html",
//            "    https://www.xr05.vip/MFStar/5510.html",
//            "    https://www.xr05.vip/XiuRen/12079.html"

//            "https://www.xr05.vip/XiuRen/11923.html",
//            "https://www.xr05.vip/XiuRen/13592.html",
//            "https://www.xr05.vip/XiuRen/12425.html",
//            "https://www.xr05.vip/XiuRen/12108.html",
//            "https://www.xr05.vip/XiuRen/12096.html",
//            "https://www.xr05.vip/XiuRen/12827.html"

//            "https://www.xr05.vip/XiuRen/13781.html",
//            "https://www.xr05.vip/XiuRen/13737.html",
//            "https://www.xr05.vip/XiuRen/14186.html",
//            "https://www.xr05.vip/XiuRen/14682.html",
//            "https://www.xr05.vip/HuaYang/8123.html",
//            "https://www.xr05.vip/XiaoYu/12210.html",
//            "https://www.xr05.vip/XiaoYu/14851.html",
//            "https://www.xr05.vip/XiuRen/14218.html",
//            "https://www.xr05.vip/IMiss/12766.html",
//            "https://www.xr05.vip/IMiss/9232.html",
//            "https://www.xr05.vip/IMiss/8045.html",
//            "https://www.xr05.vip/XiaoYu/9292.html",
//            "https://www.xr05.vip/XiaoYu/11904.html"

//            "https://www.xrmn02.top/XiuRen/2021/20217221.html",
//            "https://www.xrmn02.top/HuaYang/2021/20214840.html",
//            "https://www.xrmn02.top/XiuRen/2023/202314582.html",
//            "https://www.xrmn02.top/XiuRen/2023/202314398.html",
//            "https://www.xrmn02.top/XiuRen/2023/202314175.html",
//            "https://www.xrmn02.top/XiuRen/2023/202314030.html",
//            "https://www.xrmn02.top/XiuRen/2023/202313992.html"

            "https://www.xr05.vip/XiuRen/13450.html",
            "https://www.xr05.vip/XiuRen/13352.html",
            "https://www.xr05.vip/IMiss/7732.html",
            "https://www.xr05.vip/IMiss/12561.html",
            "https://www.xr05.vip/XiaoYu/7492.html",
            "https://www.xr05.vip/XiuRen/11180.html",
            "https://www.xr05.vip/XiuRen/14124.html",
            "https://www.xr05.vip/XiaoYu/13866.html",
            "https://www.xr05.vip/XiuRen/11351.html",
            "https://www.xr05.vip/XiuRen/9666.html",
            "https://www.xr05.vip/XiuRen/13431.html",
            "https://www.xr05.vip/XiuRen/13361.html",
            "https://www.xr05.vip/XiuRen/12486.html",
            "https://www.xr05.vip/XiuRen/12254.html",
            "https://www.xr05.vip/XiuRen/11766.html",
            "https://www.xr05.vip/IMiss/9794.html",
            "https://www.xr05.vip/XiuRen/10207.html",
            "https://www.xr05.vip/XiaoYu/10244.html",
            "https://www.xr05.vip/XiuRen/14350.html",
            "https://www.xr05.vip/XiuRen/11824.html",
            "https://www.xr05.vip/XiuRen/11275.html",
            "https://www.xr05.vip/XiuRen/13627.html",
            "https://www.xr05.vip/XiuRen/13859.html",
            "https://www.xr05.vip/XiuRen/13446.html",
            "https://www.xr05.vip/XiuRen/13081.html",
            "https://www.xr05.vip/XiuRen/12890.html",
            "https://www.xr05.vip/XiuRen/12601.html",
            "https://www.xr05.vip/YouMi/11636.html",
            "https://www.xr05.vip/XiuRen/12813.html",
            "https://www.xr05.vip/XiuRen/13386.html",
            "https://www.xr05.vip/XiuRen/13725.html",
            "https://www.xr05.vip/XiuRen/12157.html"
    );

        private static final String BASE_URL = "https://www.xr05.vip";
//    private static final String BASE_URL = "https://www.xrmn02.top";

    public static void main(String[] args) {
//        ThreadPoolUtil.pool();
//        System.out.println("总数->" + URL_LIST.size());
//        SpiderUtil spiderUtil = new SpiderUtil();
//        spiderUtil.execute(BASE_URL, 3, "D:\\Spider", URL_LIST);
//        ThreadPoolUtil.pool().shutdown();
//        ThreadPoolUtil.fastPool().shutdown();
//        ThreadPoolUtil.blockPool().shutdown();


//        File[] files = FileUtil.ls("F:\\迅雷下载\\爬取下载网站写真v2.4\\完整");
//        List<String> collect = Arrays.stream(files).map(File::getName).collect(Collectors.toList());
//        System.out.println(JSONUtil.toJsonPrettyStr(collect));
//
//        String readString = FileUtil.readString("d:\\新建 文本文档.txt", Charset.defaultCharset());
//        System.out.println(readString);
//
//        List<String> list = new ArrayList<>();
//        for (String s : collect) {
//            if (!readString.contains(s)) {
//                list.add(s);
//            }
//        }
//        System.out.println("不存在的字符串->" + JSONUtil.toJsonPrettyStr(list));
    }


}



