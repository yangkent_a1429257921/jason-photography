package com.jason.photography.api;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ArrayUtil;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.bytedeco.opencv.opencv_core.Size;
import org.junit.jupiter.api.Test;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.bytedeco.opencv.global.opencv_core.*;
import static org.bytedeco.opencv.global.opencv_imgcodecs.imread;
import static org.bytedeco.opencv.global.opencv_imgcodecs.imwrite;
import static org.bytedeco.opencv.global.opencv_imgproc.*;

/**
 * 图片处理
 *
 * @author gzc
 * @since 2024/3/5 13:51
 **/
public class ImgHandlerTest {
    //    static {
//        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//    }
    String filename = "D:\\watermarker\\20240316204533.PNG";

    @Test
    public void test11() {
        Mat image = imread(filename);

        Mat grayImage = new Mat();
        cvtColor(image,grayImage,COLOR_BGR2GRAY);
        Mat sharpenedImage = new Mat();
        Mat kernel = new Mat(3,3,CV_32F,new Scalar(0));
        kernel.create(1,1,5);
        filter2D(grayImage, sharpenedImage, -1, kernel);
        imwrite("d:\\cv.png", sharpenedImage);
    }

    @Test
    public void test10() {
        Mat mat = imread(filename);
        // 转为灰度
//        cvtColor(mat, mat, opencv_imgproc.COLOR_BGR2GRAY);
        Mat mat222 = new Mat();
//        GaussianBlur(mat, mat, new Size(3, 3), 0);
        Mat mat333 = new Mat();
//        Laplacian(mat, mat, opencv_imgproc.CV_GRAY2BGR);
        addWeighted(mat, 1.5, mat, -0.5, 0, mat);
        imwrite("d:\\cv.png", mat);



//        Mat mat = imread(filename);
//        GaussianBlur(mat, mat, new Size(5, 5), 0);
//        cvtColor(mat, mat, Imgproc.COLOR_BGR2YCrCb);
//        extractChannel(mat, mat, 0);
//        equalizeHist(mat, mat);
//        merge(mat, 1, mat);
////        merge(new ArrayList<>(Arrays.asList(mat, mat, mat)), mat);
//        cvtColor(mat, mat, Imgproc.COLOR_YCrCb2BGR);
//
//        GaussianBlur(mat, mat, new Size(0, 0), 10);
//        addWeighted(mat, 1.5, mat, -0.5, 0, mat);
//        imwrite("d:\\cv.png", mat);

    }

    /**
     * 多个图片绘制水印
     */
    @Test
    public void test1() {
        String dir = "D:\\resource";
//        List<String> authorNameList = FileUtil.loopFiles(dir);
        File[] authorNameFileArr = FileUtil.ls(dir);
        if (ArrayUtil.isNotEmpty(authorNameFileArr)) {
            for (File authorNameFile : authorNameFileArr) {
                // 过滤文件
                if (authorNameFile.isFile()) {
                    continue;
                }
                String authorName = authorNameFile.getName();

                // No.2913 白蛇 1
                File[] photographsCollectNameFileArr = authorNameFile.listFiles();
                if (ArrayUtil.isEmpty(photographsCollectNameFileArr)) {
                    continue;
                }
                for (File photographsCollectNameFile : photographsCollectNameFileArr) {

                }
            }
        }
//        System.out.println(JSONUtil.toJsonPrettyStr(authorNameList));
    }

    /**
     * 还原图片格式为webp
     */
    @Test
    public void test2() {
        String dir = "D:\\Spider";
        File[] files = FileUtil.ls(dir);
        List<String> collect = Arrays.stream(files).map(File::getName).toList();
        int number = 0;
        for (String authorName : collect) {
            System.out.println("=============================" + authorName + "===========================");
            String path = dir + File.separator + authorName;
            File[] files222 = FileUtil.ls(path);
            List<String> collect222 = Arrays.stream(files222).map(File::getName).toList();
            for (String xiezhen : collect222) {
                if (xiezhen.contains("数据记录")) {
                    continue;
                }
                System.out.println("=============================" + authorName + "     " + xiezhen + "===========================");
                String dirrrr = path + File.separator + xiezhen;
                List<File> fileList = FileUtil.loopFiles(dirrrr);
                for (File file : fileList) {
                    String type = FileUtil.getType(file);
                    String s = file.getName();
                    String[] split = s.split("\\.");
                    String fileName = split[0];
                    String filetype = split[1];
                    if (!filetype.equalsIgnoreCase(type)) {
                        String newName = fileName + "." + type;
                        FileUtil.rename(file, newName, true);
                        number++;
                    }
//                    if ("jpg".equalsIgnoreCase(type) || "png".equalsIgnoreCase(type)) {
//                        String newName = fileName.split(".")[0] + ".webp";
//                        System.out.println(newName);
//                        FileUtil.rename(file, newName, true);
//                    }
                }
            }
        }
        System.out.println("修改文件名称总数->" + number);
    }

}
