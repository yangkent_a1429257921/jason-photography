package com.jason.photography.api;

import com.jason.common.http.service.OkHttpService;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * TODO
 *
 * @author gzc
 * @since 2024/3/5 4:54
 **/
@SpringBootTest
public class SpringBootTest1 {
    @Resource
    private OkHttpService okHttpService;

    @Test
    public void test() {
        String result = okHttpService.doGet("测试", Test3.searchUrl);
        System.out.println(result);
        System.out.println("result");
    }
}
