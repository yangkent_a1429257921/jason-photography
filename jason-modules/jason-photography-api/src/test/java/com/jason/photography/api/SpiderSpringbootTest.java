package com.jason.photography.api;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.RegexPool;
import cn.hutool.core.util.ReUtil;
import cn.hutool.json.JSONUtil;
import com.jason.common.core.exception.BizException;
import com.jason.common.http.service.OkHttpService;
import com.jason.photography.api.utl.SpiderUtil;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * TODO
 *
 * @author gzc
 * @since 2024/3/5 5:10
 **/
//@SpringBootTest
@Slf4j
public class SpiderSpringbootTest {
    private static final String saveLocalDir = "D://Spider";
    @Resource
    private OkHttpService okHttpService;

    public static final String AUTHOR = "周于希";

    private static final boolean reset = false;

    @Test
    public void test1() throws Exception {
        String errFilePath = saveLocalDir + File.separator + "errPageNumberRecord.txt";
        long begin = System.currentTimeMillis();
        List<String> authorList = Arrays.asList(
                "豹纹内衣"
        );
        Map<String, List<Integer>> errMsgMap = new HashMap<>(64);
        Map<String, List<Integer>> map = null;
        if (reset) {
            String errDataJson = Assert.notBlank(FileUtil.readString(errFilePath, StandardCharsets.UTF_8), "未找到文件->" + errFilePath);
            map = JSONUtil.toBean(errDataJson, Map.class);
            if (CollUtil.isEmpty(map)) {
                System.out.println("没有需要重新执行的数据");
                return;
            }
        }
        try {
            int number = authorList.size();
            for (String author : authorList) {
                System.out.println("===================================当前执行到【" + author + "】剩余: " + --number + " =========================================");
                List<Integer> list = null;
                SpiderUtil spiderUtil = null;
                try {
                    if (reset) {
                        list = map.get(author);
                        if (CollUtil.isEmpty(list)) {
                            continue;
                        }
                    } else {
//                        if ("林美惠子".equalsIgnoreCase(author)) {
//                            list = Arrays.asList(2);
//                        }
                    }
                    spiderUtil = new SpiderUtil(author, saveLocalDir);
                    spiderUtil.searchRun(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                errMsgMap.putAll(spiderUtil.getErrMsgMap());
                Thread.sleep(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(JSONUtil.toJsonPrettyStr(errMsgMap));
        FileUtil.writeString(JSONUtil.toJsonStr(errMsgMap), errFilePath, "utf-8");
        System.out.println("人数->" + authorList.size() + ", 总耗时->" + ((System.currentTimeMillis() - begin) / 1000 / 60) + "分钟");
    }

    @Test
    public void test2() {
        SpiderUtil spiderUtil = new SpiderUtil(AUTHOR, saveLocalDir);
//        spiderUtil.setOkHttpService(okHttpService);
//        spiderUtil.readRecordFileSaveImg();
        spiderUtil.readRecordFileSaveImg(26, 1);
    }

    @Test
    public void test3() {
//        extracted(AUTHOR);
        File[] files = FileUtil.ls(saveLocalDir);
        List<String> collect = Arrays.stream(files).map(File::getName).collect(Collectors.toList());
        for (String authorName : collect) {
            if ("errPageNumberRecord.txt".equalsIgnoreCase(authorName)) {
                continue;
            }
            extracted(authorName);
        }
    }

    private void extracted(String author) {
        String path = saveLocalDir + "\\" + author;
        File[] files = FileUtil.ls(path);
        List<String> collect = Arrays.stream(files).map(File::getName).collect(Collectors.toList());
        for (String parentDirName : collect) {
            if ("数据记录.txt".equals(parentDirName)) {
                continue;
            }
            try {
                String substring = parentDirName.substring(parentDirName.length() - 5);
                List<String> list = ReUtil.getAllGroups(Pattern.compile(RegexPool.NUMBERS), substring);
                Integer targetCount = Integer.valueOf(list.get(0));
                List<String> imgFileList = FileUtil.listFileNames(path + File.separator + parentDirName);
                if (imgFileList.size() != targetCount) {
                    throw new BizException("文件夹名称->" + parentDirName + "，目标数量->" + targetCount + "，实际图片文件数量->" + imgFileList.size());
                }
            } catch (Throwable e) {
                System.out.println(parentDirName);
                e.printStackTrace();
            }
        }
        System.out.println("结束执行...............");
    }

}
