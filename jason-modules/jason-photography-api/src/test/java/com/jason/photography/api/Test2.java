package com.jason.photography.api;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.RegexPool;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.URLUtil;
import com.jason.common.core.exception.BizException;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * TODO
 *
 * @author gzc
 * @since 2024/3/3 1:54
 **/
public class Test2 {
    public static void main(String[] args) {
        String path = "D:\\Spider\\" + Test3.author;
        File[] files = FileUtil.ls(path);
        List<String> collect = Arrays.stream(files).map(File::getName).collect(Collectors.toList());
//        System.out.println(JSONUtil.toJsonPrettyStr(collect));
        for (String parentDirName : collect) {
            if ("数据记录.txt".equals(parentDirName)) {
                continue;
            }
            try {
//                System.out.println(parentDirName);
                String substring = parentDirName.substring(parentDirName.length() - 5);
                List<String> list = ReUtil.getAllGroups(Pattern.compile(RegexPool.NUMBERS), substring);
                Integer targetCount = Integer.valueOf(list.get(0));
//                System.out.println(targetCount);
                List<String> imgFileList = FileUtil.listFileNames(path + File.separator + parentDirName);
                if (imgFileList.size() != targetCount) {
                    throw new BizException("文件夹名称->" + parentDirName + "，目标数量->" + targetCount + "，实际图片文件数量->" + imgFileList.size());
                }
            } catch (Throwable e) {
                System.out.println(parentDirName);
                e.printStackTrace();
            }
        }
        System.out.println("结束执行...............");
    }
}
