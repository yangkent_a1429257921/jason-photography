package com.jason.photography.dao.entity.po;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * TODO
 *
 * @author guozhongcheng
 * @since 2023/8/29
 **/
@Data
public class AdminUser {

    /**
     * 用户主键ID
     */
    private Integer id;
    private Integer sex;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 性别描述
     */
    private String sexDesc;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像
     */
    private String headPortrait;
    private LocalDateTime createTime;
    private Integer status;
}
