package com.jason.photography.admin.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 在线用户
 *
 * @author gzc
 * @since 2023/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OnlineUser {

    /**
     * 账号名
     */
    private String userName;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 浏览器
     */
    private String browser;
    /**
     * ip
     */
    private String ip;
    /**
     * 所在地址
     */
    private String address;
    /**
     * token
     */
    private String key;
    /**
     * 登录时间
     */
    private Date loginTime;
}
