package com.jason.photography.admin.security.jwt;

import com.jason.photography.admin.security.dto.JwtUser;
import com.jason.photography.admin.service.AdminAccountService;
import com.jason.photography.dao.entity.po.AdminUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Security 核心用户接口的实现类
 *
 * @author gzc
 * @since 2023/6/25
 */
@Slf4j
@RequiredArgsConstructor
@Service("userDetailsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AdminAccountService adminAccountService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        AdminUser user = adminAccountService.getByPhone(username);
        JwtUser jwtUser = null;
        // TODO
        if (user != null) {
            jwtUser = new JwtUser(
                    user.getId(),
                    user.getPhone(),
                    user.getNickName(),
                    user.getSex() == 0 ? "男" : "女",
                    null,
                    user.getHeadPortrait(),
                    user.getCreateTime(),
                    user.getStatus(),
                    null
            );
        }
        return jwtUser;
    }


}
