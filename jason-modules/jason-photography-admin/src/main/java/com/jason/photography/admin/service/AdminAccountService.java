package com.jason.photography.admin.service;

import com.jason.photography.dao.entity.po.AdminAccount;
import com.jason.common.database.service.BaseService;
import com.jason.photography.dao.entity.po.AdminUser;

/**
 * 管理端用户表业务接口
 *
 * @author guozhongcheng
 * @since 2023-06-11
 */
public interface AdminAccountService extends BaseService<AdminAccount> {

    /**
     * 根据手机号获取用户信息
     *
     * @param username 手机号
     * @return 用户信息
     */
    AdminUser getByPhone(String username);
}
