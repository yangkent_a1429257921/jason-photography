package ${packageService};

import ${packageEntity}.${entityClassName};
import com.jason.common.database.service.BaseService;

/**
 * ${tableComment}业务接口
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${serviceClassName} extends BaseService<${entityClassName}> {

}
