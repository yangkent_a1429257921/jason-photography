package ${packageMapper};

import com.jason.common.database.mapper.CoreMapper;
import ${packageEntity}.${entityClassName};

/**
 * ${tableComment}${tableName}持久层接口
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${mapperClassName} extends CoreMapper<${entityClassName}> {

}
