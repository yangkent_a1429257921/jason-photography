package com.jason.common.file.word.extension;


import com.jason.common.file.word.dto.WordPictureInfo;

import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Word导入图片等文件上传操作接口
 *
 * @author guozhongcheng
 * @since 2023/10/9
 **/
public interface WordPictureUploadInterface {


    /**
     * 上传图片接口
     *
     * @param mappingMap 图片ID与图片本地完整路径映射集合
     * @return 图片ID与上传后的图片资源地址映射集合
     */
    Map<String, String> uploadPic(Map<String, WordPictureInfo> mappingMap);

    /**
     * 并发上传图片。
     * 如果 mappingMap.size()=30,threadGroupPicSize=10
     * 则会将mappingMap拆分成三组，每组10个，每个线程执行一组数据的上传。
     *
     * @param mappingMap         图片ID与图片本地完整路径映射集合
     * @param threadPoolExecutor 线程池
     * @return 图片ID与上传后的图片资源地址映射集合
     */
    Map<String, String> concurrentUploadPic(Map<String, WordPictureInfo> mappingMap, ThreadPoolExecutor threadPoolExecutor);

    /**
     * 获取并发上传图片的线程总数
     *
     * @return 线程总数
     */
    int getThreadNum();

}
