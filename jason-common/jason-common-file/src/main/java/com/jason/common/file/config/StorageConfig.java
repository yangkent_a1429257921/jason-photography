package com.jason.common.file.config;

import lombok.Data;

/**
 * 存储服务配置
 *
 * @author gzc
 * @since 2024/4/27
 **/
@Data
public class StorageConfig {

    private String tencentCosUrl;
    private String tencentBucket;
    private String tencentSecretId;
    private String tencentSecretKey;
    private String tencentRegion;
    private String fileDir;
    private String storagePlatform;

}
