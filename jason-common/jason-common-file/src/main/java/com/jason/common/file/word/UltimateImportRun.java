package com.jason.common.file.word;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.thread.ConcurrencyTester;
import cn.hutool.core.thread.ThreadUtil;
import com.jason.common.file.word.dto.WordImportQuestionData;
import com.jason.common.file.word.dto.WordTemplateDefinition;
import com.jason.common.file.word.exception.WordImportException;
import com.jason.common.file.word.extension.DefaultWordPictureUploadInterfaceImpl;
import com.jason.common.file.word.extension.WordPictureUploadInterface;
import com.jason.common.file.word.resolver.ConcurrentWordTextResolver;
import com.jason.common.thread.ThreadPoolUtil;

import java.io.File;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 测试导入
 *
 * @author guozhongcheng
 * @since 2023/9/28
 **/
public class UltimateImportRun {

    private final static AtomicInteger num = new AtomicInteger(1);

    public static void main(String[] args) throws Exception {
        ThreadPoolExecutor threadPoolExecutor = ThreadPoolUtil.fastPool();
        ConcurrencyTester concurrencyTester = ThreadUtil.concurrencyTest(1, () -> {
            try {
                extracted();
            } catch (Throwable e) {
                if (e instanceof WordImportException) {
                    WordImportException ex1 = (WordImportException) e;
                    System.out.println("截取段落->\n" + ex1.getSubErrorParagraph().trim());
                    System.out.println("出现错误，错误原因->" + ex1.getErrMsg());
                } else {
                    e.printStackTrace();
                }
            }
        });
        concurrencyTester.close();
        threadPoolExecutor.shutdown();
    }

    private static void extracted() throws Throwable {
//        String sss = "啊啊啊啊" + num.getAndIncrement() + ".docx";
//        String sss = "哈哈哈.docx";
        String sss = "高数-章节练习.docx";
        ConcurrentWordTextResolver concurrentWordTextResolver = new ConcurrentWordTextResolver();
        WordPictureUploadInterface wordPictureUploadInterface = new DefaultWordPictureUploadInterfaceImpl(null, 10);
        XmlWordImportQuestionContext context = new XmlWordImportQuestionContext(sss, WordTemplateDefinition.class,
                FileUtil.getInputStream("E:\\test-docx-parse\\" + sss), concurrentWordTextResolver);
        context.enableFileRemoteMapping();
        context.enableConcurrent();
        context.setThreadPoolExecutor(ThreadPoolUtil.fastPool());
        context.setWordPicUploadInterface(wordPictureUploadInterface);
        context.setLocalSaveFileBaseDir(System.getProperty("user.dir") + File.separator + "temp");
        context.setLocalSaveWordBaseDir("e://temp");
        // 执行
        List<WordImportQuestionData> list = context.execute();
        System.out.println("==============================================================");
        if (context.getExceptionStatus()) {
            System.out.println(context.getException().getMessage());
        }
//        System.out.println(JSON.toJSONString(list, true));

        System.out.println(context.printExecutePlan()
                + "文本解析计算多线程数->" + concurrentWordTextResolver.getThreadNum()
                + "\n图片上传计算多线程数->" + wordPictureUploadInterface.getThreadNum()
                + "\n" + "题目总数->" + list.size());
    }
}
