package com.jason.common.file.word.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Word导入题目数据
 *
 * @author guozhongcheng
 * @since 2023/9/28
 **/
@Data
public class WordImportQuestionData implements Serializable {
    /**
     * 试卷名称
     */
    private String paperName;
    /**
     * 大题名称/小节名称
     */
    private String paperTitleName;
    /**
     * 题目类型
     */
    private Integer questionType;
    /**
     * 题目类型中文描述
     */
    private String questionTypeDesc;
    /**
     * 背景资料
     */
    private String titleBackground;
    /**
     * 题目标题内容
     */
    private String questionTitle;
    /**
     * 题目正确答案
     */
    private String questionAnswer;
    /**
     * 题目解析
     */
    private String questionAnalysis;
    /**
     * 分数
     */
    private Double score;
    /**
     * 选项信息列表
     */
    private List<OptionData> optionDataList;
    /**
     * 排序号
     */
    private int sort;

    /**
     * 选项信息
     */
    @Data
    public static class OptionData implements Serializable {
        /**
         * 选项完整内容
         */
        private String content;
        /**
         * 选项字母
         */
        private String letter;
        /**
         * 选项文本内容
         */
        private String text;
        /**
         * 选项图片内容列表
         */
        private List<String> pictureUrlList = new ArrayList<>(16);
    }
}
