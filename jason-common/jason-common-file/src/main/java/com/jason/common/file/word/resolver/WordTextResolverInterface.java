package com.jason.common.file.word.resolver;


import com.jason.common.file.word.dto.WordImportQuestionData;
import com.jason.common.file.word.dto.WordTemplateTreeNode;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Word文本解析器接口
 *
 * @author guozhongcheng
 * @since 2023/10/12
 **/
public interface WordTextResolverInterface {

    /**
     * 解析
     *
     * @param wordTextStr          word文本字符串
     * @param templateTreeNodeList 模板树节点列表
     * @return 试题数据列表
     */
    List<WordImportQuestionData> parse(String wordTextStr, List<WordTemplateTreeNode> templateTreeNodeList);

    /**
     * 并发解析
     *
     * @param wordTextStr          word文本字符串
     * @param templateTreeNodeList 模板树节点列表
     * @param threadPoolExecutor   线程池
     * @return 试题数据列表
     * @throws Throwable 异常
     */
    List<WordImportQuestionData> concurrentParse(String wordTextStr, List<WordTemplateTreeNode> templateTreeNodeList, ThreadPoolExecutor threadPoolExecutor) throws Throwable;
}
