package com.jason.common.file.word.dto;

import com.roncoo.education.resource.common.word.anno.MappingNode;
import lombok.Data;

import java.io.Serializable;

/**
 * 模版定义对象
 *
 * @author guozhongcheng
 * @since 2023/9/28 17:31
 */
@Data
public class WordTemplateDefinition implements Serializable {
    @MappingNode(nodeName = "【第一份/第一章】")
    private Chapter chapter1;
    @MappingNode(nodeName = "【第二份/第二章】")
    private Chapter chapter2;
    @MappingNode(nodeName = "【第三份/第三章】")
    private Chapter chapter3;
    @MappingNode(nodeName = "【第四份/第四章】")
    private Chapter chapter4;
    @MappingNode(nodeName = "【第五份/第五章】")
    private Chapter chapter5;
    @MappingNode(nodeName = "【第六份/第六章】")
    private Chapter chapter6;
    @MappingNode(nodeName = "【第七份/第七章】")
    private Chapter chapter7;
    @MappingNode(nodeName = "【第八份/第八章】")
    private Chapter chapter8;
    @MappingNode(nodeName = "【第九份/第九章】")
    private Chapter chapter9;
    @MappingNode(nodeName = "【第十份/第十章】")
    private Chapter chapter10;

    @Data
    public static class Chapter {
        @MappingNode(nodeName = "【第一大题/第一节】")
        private Title t1;
        @MappingNode(nodeName = "【第二大题/第二节】")
        private Title t2;
        @MappingNode(nodeName = "【第三大题/第三节】")
        private Title t3;
        @MappingNode(nodeName = "【第四大题/第四节】")
        private Title t4;
        @MappingNode(nodeName = "【第五大题/第五节】")
        private Title t5;
        @MappingNode(nodeName = "【第六大题/第六节】")
        private Title t6;
        @MappingNode(nodeName = "【第七大题/第七节】")
        private Title t7;
        @MappingNode(nodeName = "【第八大题/第八节】")
        private Title t8;
        @MappingNode(nodeName = "【第九大题/第九节】")
        private Title t9;
        @MappingNode(nodeName = "【第十大题/第十节】")
        private Title t10;
    }

    @Data
    public static class Title {
        @MappingNode(nodeName = "【单选题】")
        private Question q1;
        @MappingNode(nodeName = "【多选题】")
        private Question q2;
        @MappingNode(nodeName = "【判断题】")
        private Question q3;
        @MappingNode(nodeName = "【填空题】")
        private Question q4;
        @MappingNode(nodeName = "【简答题】")
        private Question q5;
        @MappingNode(nodeName = "【案例分析题】")
        private Question q6;
        @MappingNode(nodeName = "【计算题】")
        private Question q7;
    }

    @Data
    public static class Question {
        @MappingNode(nodeName = "【背景资料】", questionTitleSplitter = ">>>")
        private String a;
        @MappingNode(nodeName = "【题目】")
        private String b;
        @MappingNode(nodeName = "【选项】")
        private String c;
        @MappingNode(nodeName = "【分数】", must = true)
        private String d;
        @MappingNode(nodeName = "【答案】", must = true)
        private String e;
        @MappingNode(nodeName = "【解析】", questionSplitter = "\\n\\n", must = true)
        private String f;
    }

}
