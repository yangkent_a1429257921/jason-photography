package com.jason.common.file.word.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 节点映射注解
 *
 * @author gzc
 * @since 2023/9/9
 **/
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface MappingNode {

    /**
     * 节点名称
     */
    String nodeName();

    /**
     * 试题分隔符
     */
    String questionSplitter() default "";

    /**
     * 试题题目分隔符
     */
    String questionTitleSplitter() default "";

    /**
     * 是否必须存在
     */
    boolean must() default false;
}
