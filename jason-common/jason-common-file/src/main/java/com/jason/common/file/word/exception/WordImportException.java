package com.jason.common.file.word.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Word导入运行时异常
 *
 * @author guozhongcheng
 * @since 2023/10/12 18:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WordImportException extends RuntimeException {
    /**
     * 错误码
     */
    protected Integer errCode;
    /**
     * 错误信息
     */
    protected String errMsg;
    /**
     * 截取错误段落
     */
    protected String subErrorParagraph;

    private WordImportException() {
        super();
    }

    public WordImportException(String msg, String subErrorParagraph, Throwable cause) {
        super(msg, cause);
        this.errMsg = msg;
        this.subErrorParagraph = subErrorParagraph;
    }

    public WordImportException(String errMsg, String subErrorParagraph) {
        super(errMsg);
        this.errCode = 400;
        this.errMsg = errMsg;
        this.subErrorParagraph = subErrorParagraph;
    }

    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    @Override
    public String getMessage() {
        return this.errMsg;
    }

}
