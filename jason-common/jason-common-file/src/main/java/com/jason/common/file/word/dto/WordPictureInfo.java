package com.jason.common.file.word.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Word文档中的图片信息
 *
 * @author guozhongcheng
 * @since 2023/10/9
 **/
@Data
public class WordPictureInfo implements Serializable {

    /**
     * Word文档映射图片文件唯一ID，rId79-4
     */
    private String id;

    /**
     * Word文档去重唯一ID，rId79
     */
    private String distinctId;

    /**
     * 源图片本地路径
     */
    private String sourcePictureLocalPath;

    /**
     * 新尺寸图片本地保存路径
     */
    private String newSizePictureLocalSavePath;

    /**
     * 图片在Word文档中的宽度
     */
    private int width;

    /**
     * 图片在Word文档中的高度
     */
    private int height;

    /**
     * 保存在文件服务器的资源地址
     */
    private String remoteSaveUrl;
}
