package com.jason.common.file.word.dto;

import cn.hutool.core.lang.tree.Tree;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/**
 * Word模板树
 *
 * @author guozhongcheng
 * @since 2023/9/28
 **/
@Getter
public class WordTemplateTree implements Serializable {
    /**
     * Word模板树列表
     */
    private final List<Tree<Integer>> treeList;
    /**
     * Word模板树列表
     */
    private final List<WordTemplateTreeNode> templateTreeNodeList;
    /**
     * 最深层数
     */
    private final Integer lastLevel;

    public WordTemplateTree(List<Tree<Integer>> treeList, List<WordTemplateTreeNode> templateTreeNodeList, Integer lastLevel) {
        this.treeList = treeList;
        this.templateTreeNodeList = templateTreeNodeList;
        this.lastLevel = lastLevel;
    }
}
