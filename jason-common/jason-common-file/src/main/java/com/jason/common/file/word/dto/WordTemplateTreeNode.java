package com.jason.common.file.word.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Word模板树节点
 *
 * @author guozhongcheng
 * @since 2023/9/28
 **/
@Data
public class WordTemplateTreeNode implements Serializable {
    @JSONField(ordinal = 1)
    private Integer id;
    @JSONField(ordinal = 2)
    private Integer parentId;
    @JSONField(ordinal = 3)
    private String nodeName = "";
    @JSONField(ordinal = 4)
    private String parentNodeName = "";
    private boolean lastLevel = false;
    @JSONField(ordinal = 11)
    private List<WordTemplateTreeNode> childrenList;
    /**
     * 是否是试题分割节点名称
     */
    private boolean questionSplitterStatus = false;
    /**
     * 分割符
     */
    @JSONField(ordinal = 5)
    private String questionSplitterStr;
    /**
     * 是否是试题题目分割节点名称
     */
    private boolean questionTitleSplitterStatus = false;
    /**
     * 试题题目分割符
     */
    @JSONField(ordinal = 6)
    private String questionTitleSplitterStr;
    /**
     * 是否必须
     */
    private boolean mustStatus = false;
    @JSONField(serialize = false)
    private String subText;
    @JSONField(ordinal = 7)
    private String nodeValue;
    /**
     * 节点截取的起始索引
     */
    @JSONField(ordinal = 9)
    private int beginIndex = -1;
    /**
     * 节点截取的结束索引
     */
    @JSONField(ordinal = 10)
    private int endIndex = -1;
    /**
     * 检索到的索引位置
     */
    @JSONField(ordinal = 8)
    private int indexOf = -1;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WordTemplateTreeNode that = (WordTemplateTreeNode) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
