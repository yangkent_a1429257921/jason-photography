package com.jason.common.video;


import net.sourceforge.tess4j.Tesseract;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * 读取视频字幕测试
 *
 * @author gzc
 * @since 2024/1/2 7:55
 **/
public class ReadVideoSubtitleTest {
    public static void main(String[] args) throws Exception{
        // 指定识别图片路径
        File imageFile = new File("e:\\javacv-test\\1704153876820.jpg");
        BufferedImage img = ImageIO.read(imageFile);
        Tesseract tessInst = new Tesseract();
        tessInst.setDatapath("E:\\javacv-test");
//        tessInst.setDatapath("e:\\javacv-test");
        tessInst.setLanguage("chi_tra");
//        tessInst.setLanguage("chi_sim");
//        tessInst.setLanguage("chi_all");
        String result = tessInst.doOCR(img);
        System.out.println(result);
    }
}
