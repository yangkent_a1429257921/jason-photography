package com.jason.common.video.domain;

import lombok.Data;

/**
 * TODO
 *
 * @author gzc
 * @since 2023/12/28 17:24
 **/
@Data
public class MusicMetaInfo {
    private String format;
    private Long size;
    private Long duration;
    private Integer bitRate;
    private Long sampleRate;
}
