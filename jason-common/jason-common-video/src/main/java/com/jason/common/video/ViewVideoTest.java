package com.jason.common.video;

import org.bytedeco.javacv.FFmpegFrameGrabber;

import java.util.concurrent.CompletableFuture;

/**
 * 查看视频信息
 *
 * @author gzc
 * @since 2024/1/2 5:08
 **/
public class ViewVideoTest {
    public static void main(String[] args) throws Exception {
//        ViewVideo("E:\\javacv-test\\Lolita(1997).mkv");
//        ViewVideo("E:\\javacv-test\\cut111.mkv");
//        ViewVideo("E:\\javacv-test\\cut111.mp4");
//        ViewVideo("E:\\javacv-test\\JUQ-123-C.mp4");
        ViewVideo("d:\\SSIS-233-U深夜加班最讨厌的初老上司的绝交竟然很合得来-三宫.mp4");
//        ViewVideo("E:\\javacv-test\\JUQ-123-cut.mp4");
//        ViewVideo("E:\\javacv-test\\SSIS-986-UC感觉新来的女社员很好搞定 利用前辈教育的立场来... 香水纯.mp4");
//        ViewVideo("e:\\SSIS-233-uncensored-HD.mp4");

    }

    public static void ViewVideo(String inputVideoFilePath) throws FFmpegFrameGrabber.Exception {
        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(inputVideoFilePath);
        grabber.start();
        System.out.println(grabber.getLengthInFrames());
        System.out.println(grabber.getLengthInVideoFrames());
        System.out.println(grabber.getLengthInAudioFrames());
        System.out.println(grabber.getLengthInVideoFrames() + grabber.getLengthInAudioFrames());
        System.out.println(grabber.getLengthInTime());
//        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
//            try {
//                FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber(inputVideoFilePath);
//                grabber1.start();
//                int count = 0;
//                while (grabber1.grabFrame(true, false, false, false) != null) {
//                    count++;
//                }
//                System.out.println("音频—>" + count);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> {
//            try {
//                FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber(inputVideoFilePath);
//                grabber1.start();
//                int count = 0;
//                while (grabber1.grabFrame(true, true, false, false) != null) {
//                    count++;
//                }
//                System.out.println("视频+音频—>" + count);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        CompletableFuture<Void> future3 = CompletableFuture.runAsync(() -> {
//            try {
//                FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber(inputVideoFilePath);
//                grabber1.start();
//                int count = 0;
//                while (grabber1.grabFrame(false, true, false, false) != null) {
//                    count++;
//                }
//                System.out.println("视频—>" + count);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        CompletableFuture<Void> future4 = CompletableFuture.runAsync(() -> {
//            try {
//                FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber(inputVideoFilePath);
//                grabber1.start();
//                int count = 0;
//                while (grabber1.grabFrame(true, true, true, true, true) != null) {
//                    count++;
//                }
//                System.out.println("总—>" + count);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//        CompletableFuture.allOf(future1, future2, future3, future4).join();
        grabber.stop();
//        grabber.close();
    }
}
