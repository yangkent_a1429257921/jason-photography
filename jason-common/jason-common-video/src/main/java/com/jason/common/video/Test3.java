package com.jason.common.video;

import cn.hutool.core.io.FileUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

/**
 * TODO
 *
 * @author gzc
 * @since 2024/2/27 4:10
 **/
public class Test3 {
    static class MyPanel extends Panel {

        private final Image screenImage = new BufferedImage(5472, 3648, 2);

        private final Graphics2D screenGraphic = (Graphics2D) screenImage
                .getGraphics();

        private Image backgroundImage;
        private int iconWidth;
        private int iconHeight;

        public MyPanel() {
            loadImage();
            // 设定焦点在本窗体
            setFocusable(true);
            // 设定初始构造时面板大小,这里先采用图片的大小
            setPreferredSize(new Dimension(iconWidth, iconHeight));
            // 绘制背景
            drawView();
        }

        /**
         * 载入图像
         */
        private void loadImage() {
            //获得当前类对应的相对位置image文件夹下的背景图像
            ImageIcon icon = new ImageIcon(FileUtil.readBytes("F:\\摄影\\20240212青岛\\IMG_1009.jpg"));
            iconWidth = icon.getIconWidth();
            iconHeight = icon.getIconHeight();
            System.out.println(iconWidth + " - " + iconHeight);
            //将图像实例赋给backgroundImage
            backgroundImage = icon.getImage();
        }

        private void drawView() {
            screenGraphic.drawImage(backgroundImage, 0, 0, null);
        }

        @Override
        public void paint(Graphics g) {
            g.drawImage(screenImage, 0, 0, null);
        }

    }

    static class MyFrame extends Frame {

        public MyFrame() {

            // 默认的窗体名称
            this.setTitle("显示一张图片");

            // 获得面板的实例
            MyPanel panel = new MyPanel();
            this.add(panel);
            this.addWindowListener(new WindowAdapter() {
                //设置关闭
                @Override
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
            // 执行并构建窗体设定
            this.pack();
            this.setVisible(true);
        }

    }

    public static void main(String[] args) {
        Frame frame = new MyFrame();
    }
}
