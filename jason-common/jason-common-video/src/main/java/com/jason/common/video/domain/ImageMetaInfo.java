package com.jason.common.video.domain;

import lombok.Data;

/**
 * TODO
 *
 * @author gzc
 * @since 2023/12/28 17:30
 **/
@Data
public class ImageMetaInfo {
    private Integer width;
    private Integer height;
    private Long size;
    private String format;
}
