package com.jason.common.video.domain;

import lombok.Data;

/**
 * TODO
 *
 * @author gzc
 * @since 2023/12/28 17:21
 **/
@Data
public class VideoMetaInfo {

    private Integer height;
    private Integer width;
    private Long duration;
    private String format;
    private Long size;
    private Integer bitRate;
    private String encoder;
    private String decoder;
    private Float frameRate;
    private MusicMetaInfo musicMetaInfo;

}
